import boto3
from botocore.exceptions import ClientError
from pathlib import PurePosixPath

def upload_object_into_s3(s3_path, filepath, ACCESS_ID, ACCESS_KEY, sessionToken):

   if 's3://' in filepath:
      print('SourcePath is not a valid path.' + filepath)
      raise Exception('SourcePath is not a valid path.')
   elif s3_path.find('s3://') == -1:
      print('DestinationPath is not a s3 path.' + s3_path)
      raise Exception('DestinationPath is not a valid path.')
   session = boto3.session.Session()
   s3_resource = session.resource('s3',
         aws_access_key_id=ACCESS_ID,
         aws_secret_access_key= ACCESS_KEY,
                                  aws_session_token=sessionToken)
   tokens = s3_path.split('/')
   target_key = ""
   if len(tokens) > 3:
      for tokn in range(3, len(tokens)):
         if tokn == 3:
            target_key += tokens[tokn]
         else:
            target_key += "/" + tokens[tokn]
   target_bucket_name = tokens[2]

   file_name = PurePosixPath(filepath).name
   if target_key != '':
      target_key.strip()
      key_path = target_key + "/" + file_name
   else:
      key_path = file_name
   print(("key_path: " + key_path, 'target_bucket: ' + target_bucket_name))

   try:
      # uploading Entity from local path
      with open(filepath, "rb") as file:
         s3_resource.meta.client.upload_fileobj(file, target_bucket_name, key_path)
         try:
            s3_resource.Object(target_bucket_name, key_path).wait_until_exists()
            file.close()
         except ClientError as error:
            error_code = int(error.response['Error']['Code'])
            if error_code == 412 or error_code == 304:
               print("Object didn't Upload Successfully ", target_bucket_name)
               raise error
         return "Object Uploaded Successfully"
   except Exception as error:
      print("Error in upload object function of s3 helper: " + error.__str__())
      raise error
