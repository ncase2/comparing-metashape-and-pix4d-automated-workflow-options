import Metashape
import os
import time
import rasterio
from rasterio.plot import show


def saySomething():
    print("Hello")


class Setup:
    def __init__(self, doc, outputPathFileName,
                 outputPathProjectFileNameAndExtension, outputPathRasterNameAndExtension):
        self.doc = doc
        self.outputPathFileName = outputPathFileName
        self.outputPathProjectFileNameAndExtension = outputPathProjectFileNameAndExtension
        self.outputPathRasterFileNameAndExtension = outputPathRasterNameAndExtension
    # Class implementation...


def getSetup(outputPath):
    myLicenseString = "MetashapeProfessionalLicenseKey"
    Metashape.License().activate(myLicenseString)
    doc = Metashape.Document()
    outputPathAndFileName = outputPath + "\\test1" + str(round(time.time() * 1000))
    outputPathAndFileNameAndExtension = outputPathAndFileName+".psx"
    outputPathRasterFileNameAndExtension = outputPathAndFileName+".tiff"
    setup = Setup(doc, outputPathAndFileName, outputPathAndFileNameAndExtension, outputPathRasterFileNameAndExtension)
    return setup


def getFilePaths(inputFolderPath):
    files = []
    folders = []
    for (path, dirNames, filenames) in os.walk(inputFolderPath):
        folders.extend(os.path.join(path, name) for name in dirNames)
        files.extend(os.path.join(path, name) for name in filenames)

    return files


def save(doc, outputPathAndFileNameAndExtension):
    print("Saving... " + outputPathAndFileNameAndExtension)
    doc.save(outputPathAndFileNameAndExtension)


def getTime():
    t = time.localtime()
    current_time = time.strftime("%H:%M:%S", t)
    return current_time


def elapsedTime(startTime):
    timeDifference = time.time() - startTime
    elapsed = conversion(timeDifference)
    return str(elapsed) + " seconds"


def conversion(sec):
    seconds = sec
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)


def showRaster(rasterPath):
    src = rasterio.open(rasterPath)
    image = src.read()
    show(image)


def doPhotogrammetry(path, outputPath):
    setup = getSetup(outputPath)
    originalTime = time.time()
    print("Setup at " + getTime() + " | Elapsed: " + elapsedTime(originalTime))
    save(setup.doc, setup.outputPathProjectFileNameAndExtension)
    print("Saved  at " + getTime() + " | Elapsed: " + elapsedTime(originalTime))
    chunk = setup.doc.addChunk()
    paths = getFilePaths(path)
    print("Got " + str(len(paths)) + " paths at " + getTime() + " | Elapsed: " + elapsedTime(originalTime))
    nextTime = time.time()
    chunk.addPhotos(paths)
    print("Photos added at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.matchPhotos()
    print("Photos matched at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.alignCameras(reset_alignment=True)
    print("Photos aligned at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildDepthMaps(downscale=4, filter_mode=Metashape.MildFiltering)
    print("Depth maps built at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildDenseCloud()
    print("Dense cloud made at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildModel(surface_type=Metashape.Arbitrary, interpolation=Metashape.EnabledInterpolation)
    print("Model made at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildUV(mapping_mode=Metashape.GenericMapping)
    print("UV made at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildTexture(blending_mode=Metashape.MosaicBlending, texture_size=4096)
    print("Texture made at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    print("Saving... " + setup.outputPathProjectFileNameAndExtension + " | Elapsed: " + elapsedTime(nextTime))
    setup.doc.save()
    print("Saved  at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.buildOrthomosaic(surface_data=Metashape.ModelData)
    print("Ortho made at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    nextTime = time.time()
    chunk.exportRaster(path=setup.outputPathRasterFileNameAndExtension,
                       source_data=Metashape.OrthomosaicData)
    print("Raster exported to " + setup.outputPathRasterFileNameAndExtension +
          " at " + getTime() + " | Elapsed: " + elapsedTime(nextTime))
    totalTime = time.time() - originalTime
    print("Done at " + getTime() + ". Total time: " + str(conversion(totalTime)) + "")
    print("Displaying output")
    showRaster(setup.outputPathRasterFileNameAndExtension)
